# [Sage Towers Smart Contracts](https://github.com/Sage-Towers/Smart-Contracts/tree/main/contracts)

### If you want it you can have it; Reach out and grab it Obsidian Blades!
*-Tef raid leading Hakkar kill in ZG on TB during vanilla*

# [Testnet (ChainID #90011337)](https://scan.sagetowers.com)
## [Sage Towers](https://sagetowers.com/)

[**Voidrite** 0x3ab85c1ED41A9f8275f7a446DaF5D7426e8eC839](https://scan.sagetowers.com/address/0x3ab85c1ED41A9f8275f7a446DaF5D7426e8eC839)


[**Void Ether** 0xd752df910B93750e4bA6EE1761116BB08A97A73e](https://scan.sagetowers.com/address/0xd752df910B93750e4bA6EE1761116BB08A97A73e)


[**Logical Mechanism** 0x3C856b95ADe474a321c92b5002436b7C57Fe5F96](https://scan.sagetowers.com/address/0x3C856b95ADe474a321c92b5002436b7C57Fe5F96)


[**Radiant Gift** 0x786591395a50E5f3De4eF6014D6E2012D339EbeB](https://scan.sagetowers.com/address/0x786591395a50E5f3De4eF6014D6E2012D339EbeB)

[**Radiant Gift (Minter)** 0xD2fF6D8c9989DFaD80a52393D72A4EDC51Ac1AB2](https://scan.sagetowers.com/address/0xD2fF6D8c9989DFaD80a52393D72A4EDC51Ac1AB2)
